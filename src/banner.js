// Main Banner
let url = endPoint + pathUpload + "/banners/";
function mainBanner() {
  let url = endPoint + pathUpload + "/banners";
  fetch(endPoint + pathAPI + "files/filesSlider", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ bntype: "BN", public: "Y" }),
  })
    .then((response) => {
      return response.json();
    })
    .then((json) => {
      let banner = json.filter((element) => {
        return element.bn_public === "Y";
      });
      banner.forEach((element, index) => {
        let new_ca = "";
        if (index == 0) {
          new_ca = `<div class="carousel-item active">          
            <img class="d-block w-100" src="${url}/${element.fbn_date}/${element.fbn_name}?auto=yes&bg=777&fg=555&text=${element.fbn_name}" alt="${element.fbn_name}">          
           </div>`;
        } else {
          new_ca = `<div class="carousel-item">
             <img class="d-block w-100" src="${url}/${element.fbn_date}/${element.fbn_name}?auto=yes&bg=777&fg=555&text=${element.fbn_name}" alt="${element.fbn_name}">          
             </div>`;
        }
        $("#carousel").append(new_ca);
      });
    });
}
mainBanner();

// Left Banner
function leftBanner() {
  let url = endPoint + pathUpload + "/banners";
  fetch(endPoint + pathAPI + "files/filesSlider", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ bntype: "STLF", public: "Y" }),
  })
    .then((response) => {
      return response.json();
    })
    .then((json) => {
      let banner = json;
      $("#bannerLeftA").attr(
        "href",
        `${url}/${banner[0].fbn_date}/${banner[0].fbn_name}`
      );
      $("#bannerLeftImg").attr(
        "src",
        `${url}/${banner[0].fbn_date}/${banner[0].fbn_name}`
      );
    });
}
leftBanner();

// Scroll Banner
function scrollBanner() {
  let url = endPoint + pathUpload + "/banners";
  fetch(endPoint + pathAPI + "files/filesSlider", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ bntype: "STRT", public: "Y" }),
  })
    .then((response) => {
      return response.json();
    })
    .then((json) => {
      let banner = json;
      banner.forEach((element, index) => {
        let bannerItem = "";
        if (index == 0) {
          bannerItem = `<div class="row">
            <div class="col-md-12">
              <div class="text-center">
                <a href="${url}/${element.fbn_date}/${element.fbn_name}" data-lightbox="image-3" data-title="">
                  <img src="${url}/${element.fbn_date}/${element.fbn_name}"  width="100%" class="border rounded" alt="">
                </a>
              </div>
            </div>
          </div>`;
        } else {
          bannerItem = `
          <div class="row mt-2">
          <div class="col-md-12">
            <div class="text-center">
              <a href="${url}/${element.fbn_date}/${element.fbn_name}" data-lightbox="image-3" data-title="">
                <img src="${url}/${element.fbn_date}/${element.fbn_name}"  width="100%" class="border rounded" alt="">
              </a>
            </div>
          </div>
        </div>`;
        }
        $("#scrollBanner").append(bannerItem);
      });

      // $('#bannerLeftA').attr( "href", `${url}/${banner[0].fbn_date}/${banner[0].fbn_name}` )
    });
}
scrollBanner();

announceBanner();
function announceBanner() {
  let url = endPoint + pathUpload + "/banners";
  fetch(endPoint + pathAPI + "files/filesSlider", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ bntype: "AN", public: "Y" }),
  })
    .then((response) => {
      return response.json();
    })
    .then((json) => {
      let banner = json;
     
      banner.forEach((element, index) => {
        console.log('element',element);
        let bannerItem = `<a id="lightbox-desktop" href="${url}/${element.fbn_date}/${element.fbn_name}" data-lightbox="image-announce"></a>`;
        $("#announce").append(bannerItem);
      });
      if (banner.length > 0) {
        showModal()
      }
    });
}


function showModal() {

  let alert = getCookie("alert");
  if (alert === "") {
    setTimeout(function () {
      $("#lightbox-desktop").click();
      if (
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
          navigator.userAgent
        )
      ) {
        $("#lightbox-mobile").click();
      } else {
        $("#lightbox-desktop").click();
      }
    }, 1000);
    setcookie("alert", "show");
  }
}