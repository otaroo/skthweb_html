
function getGallerys() {

  fetch(endPoint + pathAPI + 'files/filesGallerys', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ "public": "Y" , "type": "1"}) 
  })
    .then(response => {
      return response.json()
    })
    .then(json => {
      let gallery = json;  
      let activityTtile = $('.activity-items-title h5')
      let activityDate = $('.activity-items-title p')
      let activityItems = $('.activity-items')
      let activityItemsLink = $('.activity-items-grid a')
      gallery.forEach((item, index) => {
        $(activityTtile[index]).text(item.gl_title);
        $(activityDate[index]).text(moment(item.gl_date).format("Do MMMM  YYYY"));
        $(activityItems[index]).css('background-image','linear-gradient(0deg, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('+endPoint + pathUpload + pathGalleries + item.fg_date + '/' + item.fg_name +')') ;
        $(activityItemsLink[index]).attr('href', "?page=gallery&glid=" + item.gl_id);

        // $('#act_id_' + (index + 1)).attr('href', "?page=gallery&glid=" + item.gl_id);
        // $('#act_title_' + (index + 1)).text(item.gl_title);
        // $('#act_date_' + (index + 1)).text(moment(item.gl_date).format("Do MMMM  YYYY"));
        // $('#act_desc_' + (index + 1)).text(item.gl_description);
        // $('#act_img_' + (index + 1)).attr('src', endPoint + pathUpload + pathGalleries + item.fg_date + '/' + item.fg_name);
        // if (item.fg_front_css) {
        //   $('#act_img_' + (index + 1)).css( JSON.parse(item.fg_front_css));
        // }
      });
    })
}

function getGallerysKnowledge() {
  
  fetch(endPoint + pathAPI + 'files/filesGallerys', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ "public": "Y" , "type": "3"}) 
  })
    .then(response => {
      return response.json()
    })
    .then(json => {
      let gallery = json;  
      let knowledgeImg = $('.news-knowledge-item img');
      let knowledgeLimk = $('.news-knowledge-item a');
      gallery.forEach((item, index) => {
        let imgPath = ''+endPoint + pathUpload + pathGalleries + item.fg_date + '/' + item.fg_name +'';
        $(knowledgeLimk[index]).attr('href', imgPath );
        $(knowledgeImg[index]).attr('src', imgPath );

      });
    }) 
}





function getGallerysbyid(glid) {
  // let url = 'http://192.168.10.20/skthadmin/uploads/galleries/'
  let url = endPoint + pathUpload + "/galleries";
  let data = {
    "glid": glid,
  }
  fetch(endPoint + pathAPI + 'files/files_gallery', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data)
  })
    .then(response => {
      return response.json()
    })
    .then(json => {
      let gallery = json;
      let path = Math.ceil(gallery.length / 4);
      let col = 1;
      let fileItem = '';
      let columnImg = 1
      for (let index = 0; index < gallery.length; index++) {
        const element = gallery[index];
        // fileItem += `<img src="${url}/${element.fg_date}/${element.fg_name}"  style="width:100%">`

        fileItem += `<a  href="${url}/${element.fg_date}/${element.fg_name}" data-lightbox="${element.fg_gl_id}" data-title="">
        <img  src="${url}/${element.fg_date}/${element.fg_name}"  width="100%" class="border rounded" alt=""></a>`
        $('#c' + columnImg).append(fileItem);
        if (columnImg == 4) {
          columnImg = 1
        } else {
          columnImg = columnImg + 1
        }
        fileItem = ''
      }
    })
}
$.urlParam = function (name) {
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  if (results == null) {
    return null;
  }
  return decodeURI(results[1]) || 0;
}
var page = $.urlParam('page');
var glid = $.urlParam('glid');

switch (String(page)) {
  case "gallery":
    getGallerysbyid(glid);
    getgallery(glid)
    break;
  case "searchgallery":
    initTable();
    initDatepicker();
    break;
  default:
    break;
}

function getgallery(id) {
  let sendDate = {
    "id": id
  }
  fetch(endPoint + pathAPI + 'gallery/gallerybyid', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(sendDate)
  })
    .then(response => {
      return response.json()
    })
    .then(json => {
      let gallery = json;
      console.log(gallery);
      $('#galleryTitle').text(gallery.gl_title);
      $('#galleryDesc').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + gallery.gl_desc);
      $('#galleryDate').html('<span style="font-size: 1em;"><i class="far fa-calendar-alt"></i></span>  ' + moment(gallery.gl_date).format('ll'));
    })
}

let tablegallery;
function initTable() {
  setTimeout(() => {
    tablegallery = $('#tablegallery').DataTable({
      "order": [[1, "desc"]],
      "lengthChange": false,
      "pageLength": 20,
      "language": {
        "info": "หน้า _PAGE_ ทั้งหมด _PAGES_ หน้า",
        "search": "ค้นหา",
        "zeroRecords": "ไม่พบที่ค้นหา",
        "infoEmpty": "0 ข่าว",
        "paginate": {
          "first": "First",
          "last": "Last",
          "next": "ถัดไป",
          "previous": "ย้อนกลับ"
        },
        "processing": "กำลังโหลด",
      },
      "processing": true,
      "serverSide": true,
      ajax: function (data, callback) {
        let startDate = moment($('#startDate').datepicker('getDates')[0]).format("YYYY-MM-DD")
        let endDate = moment($('#endDate').datepicker('getDates')[0]).format("YYYY-MM-DD")
        let newsData = {
          "page": ((data.length + data.start) / data.length),
          "limit": data.length,
          "title": data.search.value,
          "mode": "select",
          "datefrom": startDate,
          "dateto": endDate,
        }
        fetch(endPoint + pathAPI + '/gallery/galleriesindex.php', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(newsData),
        }).then(response => response.json())
          .then(json => {
            let news = json;
            console.log(json);
            
            // console.log(news);
            callback({
              recordsTotal: news.recordsTotal[0].TOTAL,
              recordsFiltered: news.recordsTotal[0].TOTAL,
              data: news.data.filter((element) => {
                return element.gl_public === "Y"
              }),
            });
          })

      },
      "columns": [
        {
          "data": function (p) {
            let url = endPoint + pathUpload + "/galleries";

            return `<a  href="${url}/${p.gl_file_index}" data-lightbox="${p.gl_id}" data-title="">
            <img  src="${url}/${p.gl_file_index}"  width="100%" class="border rounded" alt=""></a>`
          }
        },
        {
          "data": function (p) {
            return `<a href="?page=gallery&glid=${p.gl_id}" >${p.gl_title}</a>`;
          }
        },
        // { "data": "NEWSTYPE" },
        {
          "data": function (p) {
            return moment(p.gl_date).format('DD MMM YYYY');
          }
        },
      ]
    });
  }, 700);

}

function initDatepicker() {
  setTimeout(() => {
    $('.datePicker').datepicker({
      language: "th",
      autoclose: true,
      todayHighlight: true,
      format: "dd/mm/yyyy"
    });
    let setStartDate = moment().subtract(3, 'month').format('DD-MM-YYYY');
    let setEndDate = moment().format('DD-MM-YYYY');
    $('#startDate').datepicker('setDate', setStartDate);
    $('#endDate').datepicker('setDate', setEndDate);
    $('.datePicker').datepicker()
      .on('changeDate', function (e) {
        let startDate = moment($('#startDate').datepicker('getDates')[0])
        let endDate = moment($('#endDate').datepicker('getDates')[0])
        if (endDate._i === undefined) {
          $('#endDate').datepicker('setDate', moment($('#startDate').datepicker('getDates')[0]).format("DD/MM/YYYY"));

        }
        if (startDate._i === undefined) {
          $('#startDate').datepicker('setDate', moment($('#endDate').datepicker('getDates')[0]).format("DD/MM/YYYY"));

        }
        let calDate = endDate - startDate;
        if (calDate < 0) {
          $('#startDate').datepicker('setDate', moment($('#endDate').datepicker('getDates')[0]).format("DD/MM/YYYY"));
          return
        }
        if (calDate >= 0) {
          tablegallery.ajax.reload();
          return
        }

      });
  }, 500);

}