$(document).ready(function () {
  // setting
  moment.locale("th");
  // testAPI();
  getGallerysKnowledge();
  // getGallerysKnowledge();
  // ดึงกิจกรรม
  getGallerys();

  //ดึงข่าวประกาศ
  getNews();

  //ดึงข่าวประกาศเภสัช
  getNewsPharmacy();

  //ดึงตารางข่าว ITA
  initTableITA();

  // scollItem();
});

function scollItem() {
  console.log("scollItem");
  let tl = gsap.timeline({
    scrollTrigger: {
      trigger: ".information",
      strat: "top center",
    },
  });
  tl.from(".information-title", { x: 200, opacity: 0, duration: 1 }).from(
    ".information-items",
    { y: 200, opacity: 0, duration: 1 },
    "-=1"
  );
}

$(window).scroll(function () {
  if ($(document).scrollTop() <= 150) {
    $(".navbar-top").removeClass("navbar-scroll ");
  } else {
    $(".navbar-top").addClass("navbar-scroll ");
  }
});

const menuBtn = document.querySelector(".burger");
let navFlex = document.querySelector(".nav-menu");
menuBtn.addEventListener("click", function () {
  navFlex.classList.toggle("active");
  this.classList.toggle("change");
  // navFlex.style.display = "flex";
});

if (
  /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
    navigator.userAgent
  )
) {
  // some code..
  console.log("Mobile " + navigator.userAgent);
} else {
  let marker = document.querySelector("#marker");
  let listNav = document.querySelectorAll(".nav-menu li a");
  for (let index = 0; index < listNav.length; index++) {
    const element = listNav[index];
    element.addEventListener("mouseover", (e) => {
      marker.style.left = e.target.offsetLeft + "px";
      marker.style.width = e.target.offsetWidth + "px";
    });
  }
}

let browser = document.querySelector("#browser");
browser.style.fontSize = "0.6rem";
browser.innerHTML =
  navigator.userAgent + ` Screen : ${screen.width}X${screen.height}`;

setTimeout(function () {
  btnEffect();
}, 3000);

function btnEffect() {
  const btn = document.querySelector(".information-items").children;
  let arr = [].slice.call(btn);
  for (let index = 0; index < btn.length; index++) {
    const element = arr[index];
    arr[index].onmousemove = function (e) {
      const x = e.pageX - arr[index].offsetLeft;
      const y = e.pageY - arr[index].offsetTop;
      arr[index].style.setProperty("--x", x + "px");
      arr[index].style.setProperty("--y", y + "px");
    };
  }
}
