function getNews() {
  fetch(endPoint + pathAPI + "news/indexnews", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ type: "1" }),
  })
    .then((response) => response.json())
    .then((json) => {
      json.forEach((i, index) => {
        if ((i.news_public = "Y")) {
          if (i.news_title.length > 120) {
            i.news_title = i.news_title.substring(0, 120) + "...";
          }
          let new_el = ` <a  href="?page=viewnew&news_id=${i.news_id}" 
            class="list-group-item list-group-item-action flex-column align-items-center list-group-flush list-group-item-bn" style="padding: 0rem .5rem;"> 
            <div class="d-flex w-100 justify-content-between align-items-center"> 
            <p class="mb-1" style="overflow: hidden; height: 24px">${
              index + 1
            }.${i.news_title}</p> 
            <small class="text-muted"><span style="font-size: 1em;"><i class="far fa-calendar-alt"></i></span>${moment(
              i.news_datepost
            ).format("DD MMM YYYY")}</small> 
              </div> 
               </a> `;
          $("#new_tab_1 > .list-group").append(new_el);
          let mblist = `<li class="list-group-item"><a href="?page=viewnew&news_id=${
            i.news_id
          }">${i.news_title} <br>
        <small class="text-muted"><span style="font-size: 1em;"><i class="far fa-calendar-alt"></i></span> ${moment(
          i.news_datepost
        ).format("DD MMM YYYY")}</small></a></li>`;
          $("#mbnews1").append(mblist);
        }
      });
    });

  fetch(endPoint + pathAPI + "news/indexnews", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ type: "2" }),
  })
    .then((response) => response.json())
    .then((json) => {
      json.forEach((i, index) => {
        if ((i.news_public = "Y")) {
          if (i.news_title.length > 120) {
            i.news_title = i.news_title.substring(0, 120) + "...";
          }
          let new_el = ` <a  href="?page=viewnew&news_id=${i.news_id}" 
          class="list-group-item list-group-item-action flex-column align-items-center list-group-flush list-group-item-bn" style="padding: 0rem .5rem;"> 
          <div class="d-flex w-100 justify-content-between align-items-center"> 
          <p class="mb-1" style="overflow: hidden; height: 24px">${index + 1}.${
            i.news_title
          }</p> 
          <small class="text-muted"><span style="font-size: 1em;"><i class="far fa-calendar-alt"></i></span>${moment(
            i.news_datepost
          ).format("DD MMM YYYY")}</small> 
            </div> 
             </a> `;
          $("#new_tab_2 > .list-group").append(new_el);
          let mblist = `<li class="list-group-item"><a href="?page=viewnew&news_id=${
            i.news_id
          }">${i.news_title} <br>
        <small class="text-muted"><span style="font-size: 1em;"><i class="far fa-calendar-alt"></i></span> ${moment(
          i.news_datepost
        ).format("DD MMM YYYY")}</small></a></li>`;

          $("#mbnews2").append(mblist);
        }
      });
    });

  fetch(endPoint + pathAPI + "news/indexnews", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ type: "3" }),
  })
    .then((response) => response.json())
    .then((json) => {
      json.forEach((i, index) => {
        if ((i.news_public = "Y")) {
          if (i.news_title.length > 120) {
            i.news_title = i.news_title.substring(0, 120) + "...";
          }
          let new_el = ` <a  href="?page=viewnew&news_id=${i.news_id}" 
          class="list-group-item list-group-item-action flex-column align-items-center list-group-flush list-group-item-bn" style="padding: 0rem .5rem;"> 
          <div class="d-flex w-100 justify-content-between align-items-center"> 
          <p class="mb-1" style="overflow: hidden; height: 24px">${index + 1}.${
            i.news_title
          }</p> 
          <small class="text-muted"><span style="font-size: 1em;"><i class="far fa-calendar-alt"></i></span>${moment(
            i.news_datepost
          ).format("DD MMM YYYY")}</small> 
            </div> 
             </a> `;
          $("#new_tab_3 > .list-group").append(new_el);
          let mblist = `<li class="list-group-item"><a href="?page=viewnew&news_id=${
            i.news_id
          }">${i.news_title} <br>
        <small class="text-muted"><span style="font-size: 1em;"><i class="far fa-calendar-alt"></i></span> ${moment(
          i.news_datepost
        ).format("DD MMM YYYY")}</small></a></li>`;

          $("#mbnews3").append(mblist);
        }
      });
    });



  fetch(endPoint + pathAPI + "news/indexnews", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ type: "5" }),
  })
    .then((response) => response.json())
    .then((json) => {
      json.forEach((i, index) => {
        if ((i.news_public = "Y")) {
          if (i.news_title.length > 120) {
            i.news_title = i.news_title.substring(0, 120) + "...";
          }
          let new_el =
            ' <a  href="?page=viewnew&news_id=' +
            i.news_id +
            '" ' +
            ' class="list-group-item list-group-item-action flex-column align-items-center list-group-flush list-group-item-bn" style="padding: 0rem .5rem;"> ' +
            ' <div class="d-flex w-100 justify-content-between align-items-center"> ' +
            ' <p class="mb-1">' +
            (index + 1) +
            ". " +
            i.news_title +
            "</p> " +
            ' <small class="text-muted"><span style="font-size: 1em;"><i class="far fa-calendar-alt"></i></span> ' +
            moment(i.news_datepost).format("DD MMM YYYY");
          +"</small> " + " </div> " + " </a> ";
          $("#new_tab_5 > .list-group").append(new_el);
          let mblist = `<li class="list-group-item"><a href="?page=viewnew&news_id=${
            i.news_id
          }">${i.news_title} <br>
        <small class="text-muted"><span style="font-size: 1em;"><i class="far fa-calendar-alt"></i></span> ${moment(
          i.news_datepost
        ).format("DD MMM YYYY")}</small></a></li>`;

          $("#mbnews5").append(mblist);
        }
      });
    });

  fetch(endPoint + pathAPI + "news/indexnews", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ type: "9" }),
  })
    .then((response) => response.json())
    .then((json) => {
      json.forEach((i, index) => {
        if ((i.news_public = "Y")) {
          if (i.news_title.length > 120) {
            i.news_title = i.news_title.substring(0, 120) + "...";
          }
          let new_el =
            ' <a  href="?page=viewnew&news_id=' +
            i.news_id +
            '" ' +
            ' class="list-group-item list-group-item-action flex-column align-items-center list-group-flush list-group-item-bn" style="padding: 0rem .5rem;"> ' +
            ' <div class="d-flex w-100 justify-content-between align-items-center"> ' +
            ' <p class="mb-1">' +
            (index + 1) +
            ". " +
            i.news_title +
            "</p> " +
            ' <small class="text-muted"><span style="font-size: 1em;"><i class="far fa-calendar-alt"></i></span> ' +
            moment(i.news_datepost).format("DD MMM YYYY");
          +"</small> " + " </div> " + " </a> ";
          $("#new_tab_6 > .list-group").append(new_el);
          let mblist = `<li class="list-group-item"><a href="?page=viewnew&news_id=${
            i.news_id
          }">${i.news_title} <br>
        <small class="text-muted"><span style="font-size: 1em;"><i class="far fa-calendar-alt"></i></span> ${moment(
          i.news_datepost
        ).format("DD MMM YYYY")}</small></a></li>`;

          $("#mbnews6").append(mblist);
        }
      });
    });



  table = $("#table_tab_7").DataTable({
    order: [[1, "desc"]],
    lengthChange: false,
    pageLength: 20,
    language: {
      info: "หน้า _PAGE_ ทั้งหมด _PAGES_ หน้า",
      search: "ค้นหา",
      zeroRecords: "ไม่พบข่าวที่ค้นหา",
      infoEmpty: "0 ข่าว",
      paginate: {
        first: "First",
        last: "Last",
        next: "ถัดไป",
        previous: "ย้อนกลับ",
      },
      processing: "กำลังโหลด",
    },
    processing: true,
    serverSide: true,
    ajax: function (data, callback) {
      let startDate = moment().subtract(12, 'month').format("YYYY-MM-DD");
      let endDate = moment().format("YYYY-MM-DD");

      let newsData = {
        page: (data.length + data.start) / data.length,
        limit: data.length,
        title: data.search.value,
        type: 8,
        datefrom: startDate,
        dateto: endDate,
      };
      fetch(endPoint + pathAPI + "/news/newsindex.php", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(newsData),
      })
        .then((response) => response.json())
        .then((json) => {
          let news = json;
          // console.log(news);
          callback({
            recordsTotal: news.recordsTotal[0].TOTAL,
            recordsFiltered: news.recordsTotal[0].TOTAL,
            data: news.data,
          });
        });
    },
    columns: [
      {
        data: function (p) {
          return `<a href="?page=viewnew&news_id=${p.ID}" >${p.TITLE}</a>`;
        },
      },
      // { "data": "NEWSTYPE" },
      {
        data: function (p) {
          return moment(p.DATEPOST).format("DD MMM YYYY");
        },
      },
    ],
  });

  $("#table_tab_4").DataTable({
    order: [[1, "desc"]],
    lengthChange: false,
    pageLength: 20,
    language: {
      info: "หน้า _PAGE_ ทั้งหมด _PAGES_ หน้า",
      search: "ค้นหา",
      zeroRecords: "ไม่พบข่าวที่ค้นหา",
      infoEmpty: "0 ข่าว",
      paginate: {
        first: "First",
        last: "Last",
        next: "ถัดไป",
        previous: "ย้อนกลับ",
      },
      processing: "กำลังโหลด",
    },

  });
  
}

$.urlParam = function (name) {
  var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
    window.location.href
  );
  if (results == null) {
    return null;
  }
  return decodeURI(results[1]) || 0;
};
var page = $.urlParam("page");
var news_id = $.urlParam("news_id");

switch (String(page)) {
  case "viewnew":
    getnew(news_id);
    getfilenew(news_id);
    break;
  case "searchnews":
    // getAllNews("1");
    initTable();
    initDatepicker();
    console.log("searchnews");
    break;

  default:
    break;
}

function getnew(news_id) {
  fetch(endPoint + pathAPI + "/news/newsbyid.php", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ id: news_id }),
  })
    .then((response) => response.json())
    .then((json) => {
      let news = json;
      console.log(news);
      $("#newTitle").text(news.news_title);
      $("#metaTitle").attr("content", news.news_title);
      $("#newsDate").html(
        '<span style="font-size: 1em;"><i class="far fa-calendar-alt"></i></span> ' +
          news.news_datepost
      );
      $("#vnUser").html(
        '<span style="font-size: 1em;"><i class="far fa-user"></i></span> AdminIT'
      );
    });
}
function getfilenew(news_id) {
  let url = endPoint + pathUpload + "/news";
  fetch(endPoint + pathAPI + "/files/files.php", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ newsid: news_id }),
  })
    .then((response) => response.json())
    .then((json) => {
      let news = json.reverse();
      news.forEach((item, index) => {
        let fileItem = "";
        if (index == 0) {
          fileItem = `<li class="list-group-item active  itemNew" data-f_name="${item.f_name}" data-f_date="${item.f_date}" >${item.f_oldname}</li>`;
          $("#pdfView").attr("src", `${url}/${item.f_date}/${item.f_name}`);
        } else {
          fileItem = `<li class="list-group-item itemNew" data-f_name="${item.f_name}" data-f_date="${item.f_date}" >${item.f_oldname}</li>`;
        }
        $("#listNews").append(fileItem);
      });
      $(".itemNew").click(function () {
        $(".itemNew").removeClass("active");
        let f_name = $(this).attr("data-f_name");
        let f_date = $(this).attr("data-f_date");
        $(this).addClass("active");
        $("#pdfView").attr("src", `${url}/${f_date}/${f_name}`);
        console.log(`${url}/${f_date}/${f_name}`);
      });
    });
}

setTimeout(() => {
  $("#newsType").change(async function () {
    let queryParams = new URLSearchParams(window.location.search);
    queryParams.set("type", $("#newsType").children("option:selected").val());
    queryParams.set("pageNum", "1");
    history.replaceState(null, null, "?" + queryParams.toString());
    table.ajax.reload();
  });
}, 1000);

let table;
function initTable() {
  setTimeout(() => {
    let queryParams = new URLSearchParams(window.location.search);
    let type = $.urlParam("type");
    let pageNum = $.urlParam("pageNum");
    if (type) {
      $("#newsType").val(type);
    } else {
      queryParams.set("type", "1");
    }
    if (pageNum) {
      setTimeout(() => {
        table.page(parseInt(--pageNum)).draw(false);
      }, 2000);
    } else {
      queryParams.set("pageNum", "1");
    }
    history.replaceState(null, null, "?" + queryParams.toString());

    table = $("#tablenews").DataTable({
      order: [[1, "desc"]],
      lengthChange: false,
      pageLength: 20,
      language: {
        info: "หน้า _PAGE_ ทั้งหมด _PAGES_ หน้า",
        search: "ค้นหาข่าว",
        zeroRecords: "ไม่พบข่าวที่ค้นหา",
        infoEmpty: "0 ข่าว",
        paginate: {
          first: "First",
          last: "Last",
          next: "ถัดไป",
          previous: "ย้อนกลับ",
        },
        processing: "กำลังโหลด",
      },
      processing: true,
      serverSide: true,
      ajax: function (data, callback) {
        let startDate = moment(
          $("#startDate").datepicker("getDates")[0]
        ).format("YYYY-MM-DD");
        let endDate = moment($("#endDate").datepicker("getDates")[0]).format(
          "YYYY-MM-DD"
        );

        let newsData = {
          page: (data.length + data.start) / data.length,
          limit: data.length,
          title: data.search.value,
          type: $("#newsType").children("option:selected").val() || 1,
          datefrom: startDate,
          dateto: endDate,
        };
        fetch(endPoint + pathAPI + "/news/newsindex.php", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(newsData),
        })
          .then((response) => response.json())
          .then((json) => {
            let news = json;
            // console.log(news);
            callback({
              recordsTotal: news.recordsTotal[0].TOTAL,
              recordsFiltered: news.recordsTotal[0].TOTAL,
              data: news.data,
            });
          });
      },
      columns: [
        {
          data: function (p) {
            return `<a href="?page=viewnew&news_id=${p.ID}" >${p.TITLE}</a>`;
          },
        },
        // { "data": "NEWSTYPE" },
        {
          data: function (p) {
            return moment(p.DATEPOST).format("DD MMM YYYY");
          },
        },
      ],
    });

    setTimeout(() => {
      table.on("page.dt", function () {
        let info = table.page.info();
        let queryParams = new URLSearchParams(window.location.search);
        queryParams.set("pageNum", parseInt(++info.page));
        history.replaceState(null, null, "?" + queryParams.toString());
        // $('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
      });
    }, 2000);
  }, 700);
}

$.urlParam = function (name) {
  let results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
    window.location.href
  );
  if (results == null) {
    return null;
  }
  return decodeURI(results[1]) || 0;
};

function initDatepicker() {
  setTimeout(() => {
    $(".datePicker").datepicker({
      language: "th",
      autoclose: true,
      todayHighlight: true,
      format: "dd/mm/yyyy",
    });
    let setStartDate = moment().subtract(3, "month").format("DD-MM-YYYY");
    let setEndDate = moment().format("DD-MM-YYYY");
    $("#startDate").datepicker("setDate", setStartDate);
    $("#endDate").datepicker("setDate", setEndDate);
    $(".datePicker")
      .datepicker()
      .on("changeDate", function (e) {
        let startDate = moment($("#startDate").datepicker("getDates")[0]);
        let endDate = moment($("#endDate").datepicker("getDates")[0]);
        if (endDate._i === undefined) {
          $("#endDate").datepicker(
            "setDate",
            moment($("#startDate").datepicker("getDates")[0]).format(
              "DD/MM/YYYY"
            )
          );
        }
        if (startDate._i === undefined) {
          $("#startDate").datepicker(
            "setDate",
            moment($("#endDate").datepicker("getDates")[0]).format("DD/MM/YYYY")
          );
        }
        let calDate = endDate - startDate;
        if (calDate < 0) {
          $("#startDate").datepicker(
            "setDate",
            moment($("#endDate").datepicker("getDates")[0]).format("DD/MM/YYYY")
          );
          return;
        }
        if (calDate >= 0) {
          table.ajax.reload();
          return;
        }
      });
  }, 500);
}
