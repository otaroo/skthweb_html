$.urlParam = function (name) {
  var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
    window.location.href
  );
  if (results == null) {
    return null;
  }
  return decodeURI(results[1]) || 0;
};
var page = $.urlParam("page");
var news_id = $.urlParam("news_id");
if (page) {
  logViewPage(page, news_id);
}
if (window.location.protocol === "https:") {
  window.location.href = "http://www.skth.go.th/";
}

switch (String(page)) {
  case "index":
    break;

  case "history":
    getPage(page);
    break;

  case "visionmission":
    getPage(page);
    break;

  case "kumkwan":
    getPage(page);
    break;

  case "president":
    getPage(page);
    break;

  case "structure":
    getPage(page);
    break;

  case "location":
    getPage(page);
    break;

  case "gallery":
    getPage(page);
    break;

  case "searchgallery":
    getPage(page);
    break;

  case "viewnew":
    getPage(page);
    break;

  case "searchnews":
    getPage(page);
    break;

  case "download":
    getPage(page);
    break;

  case "doctor":
    getPage(page);
    break;

  case "roomservice":
    getPage(page);
    break;

  case "appointment":
    getPage(page);
    break;

  case "pharmacy":
    getPage(page);
    break;

  case "ita":
    getPage(page);
    break;

  case "eb":
    getPage(page);
    break;

  case "null":
    break;

  default:
    break;
}

function getPage(params) {
  $("#main").hide();
  $.ajax({
    type: "GET",
    url: window.location.pathname.split("/")[0] + params + ".html",
    success: function (data) {
      $("#main").html(data);
      $("#main").show();
    },
  });
}





function logViewPage(page, id = "") {
  console.log(page);
  fetch(endPoint + pathAPI + "log/view", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ page: page, id: id }),
  })
    .then((response) => response.json())
    .then((json) => {
      
    });
}

function setcookie(cookieName, cookieValue) {
  var today = new Date();
  var expire = new Date();
  expire.setTime(today.getTime() + 1 * 24 * 60 * 60 * 1000);
  // expire.setTime(today.getTime() + 3600000*24*7);
  document.cookie =
    cookieName +
    "=" +
    encodeURI(cookieValue) +
    ";expires=" +
    expire.toGMTString();
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
