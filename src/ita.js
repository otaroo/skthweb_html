let tableita;
let tableitaeb;
function initTableITA() {
  setTimeout(() => {
    tableita = $("#table-ita").DataTable({
      //   order: [[0, "desc"]],
      lengthChange: false,
      searching: false,
      info: false,
      pageLength: 10,
      language: {
        info: "หน้า _PAGE_ ทั้งหมด _PAGES_ หน้า",
        search: "ค้นหาข่าว",
        zeroRecords: "ไม่พบข่าวที่ค้นหา",
        infoEmpty: "0 ข่าว",
        paginate: {
          first: "First",
          last: "Last",
          next: "ถัดไป",
          previous: "ย้อนกลับ",
        },
        processing: "กำลังโหลด",
      },
      processing: true,
      serverSide: true,
      ajax: function (data, callback) {
        let newsData = {
          page: (data.length + data.start) / data.length,
          limit: data.length,
          title: data.search.value,
          type: 9,
          datefrom: moment().subtract(1, "years").format("YYYY-MM-DD"),
          dateto: moment().format("YYYY-MM-DD"),
        };
        fetch(endPoint + pathAPI + "/news/newsindex.php", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(newsData),
        })
          .then((response) => response.json())
          .then((json) => {
            let news = json;
            // console.log(news);
            callback({
              recordsTotal: news.recordsTotal[0].TOTAL,
              recordsFiltered: news.recordsTotal[0].TOTAL,
              data: news.data,
            });
          });
      },
      columns: [
        {
          data: function (p, type, row) {
            // <a href="?page=viewnew&news_id=${p.ID}" >${p.TITLE}</a>
            if (p.TITLE.length > 120) {
              p.TITLE = p.TITLE.substring(0, 120) + "...";
            }
            return `
            <a
                href="?page=viewnew&news_id=${p.ID}"
                class="list-group-item list-group-item-action flex-column align-items-center list-group-flush list-group-item-bn"
                style="padding: 0rem .5rem;"
                >
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <p class="mb-1">${p.TITLE}</p>
                    <small class="text-muted">
                    <span style="font-size: 1em;">
                        <i class="far fa-calendar-alt"></i>
                    </span>${moment(p.DATEPOST).format("DD MMM YYYY")}
                    </small>
                </div>
            </a>
            `;
          },
        },
        // { "data": "NEWSTYPE" },
        // {
        //   "data": function (p) {
        //     return moment(p.DATEPOST).format('DD MMM YYYY');
        //   }
        // },
      ],
      drawCallback: function (settings) {
        $("#table-ita thead").remove();
      },
    });
  }, 700);

  setTimeout(() => {
    let eb = $.urlParam("eb");
    let year = $.urlParam("year");
    
    tableita = $("#tableita").DataTable({
      order: [[0, "desc"]],
      lengthChange: false,
      searching: false,
      info: false,
      pageLength: 100,
      language: {
        info: "หน้า _PAGE_ ทั้งหมด _PAGES_ หน้า",
        search: "ค้นหาข่าว",
        zeroRecords: "ไม่พบข่าวที่ค้นหา",
        infoEmpty: "0 ข่าว",
        paginate: {
          first: "First",
          last: "Last",
          next: "ถัดไป",
          previous: "ย้อนกลับ",
        },
        processing: "กำลังโหลด",
      },
      processing: true,
      serverSide: true,
      ajax: function (data, callback) {
        let newsData = {
          page: (data.length + data.start) / data.length,
          limit: data.length,
          title: data.search.value,
          type: 9,
          eb: eb || 1,
          year: year,
          datefrom: moment().subtract(10, "years").format("YYYY-MM-DD"),
          dateto: moment().format("YYYY-MM-DD"),
        };
        fetch(endPoint + pathAPI + "/news/newsindex.php", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(newsData),
        })
          .then((response) => response.json())
          .then((json) => {
            let news = json;
            // console.log(news);
            callback({
              recordsTotal: news.recordsTotal[0].TOTAL,
              recordsFiltered: news.recordsTotal[0].TOTAL,
              data: news.data,
            });
          });
      },
      columns: [
        {
          data: function (p, type, row) {
            // <a href="?page=viewnew&news_id=${p.ID}" >${p.TITLE}</a>
            if (p.TITLE.length > 120) {
              p.TITLE = p.TITLE.substring(0, 120) + "...";
            }
            return `
            <a
                href="?page=viewnew&news_id=${p.ID}"
                class=" flex-column align-items-center list-group-flush list-group-item-bn"
                >
                <div class="d-flex w-100 justify-content-between align-items-center">
                    <p class="mb-1">${p.TITLE}</p>
                </div>
            </a>
            `;
          },
        },
        {
          data: function (p, type, row) {
            return `  <small class="text-muted"><i class="far fa-calendar-alt"></i> ${moment(
              p.DATEPOST
            ).format("DD MMM YYYY")}</small>`;
          },
        },
        // { "data": "NEWSTYPE" },
        // {
        //   "data": function (p) {
        //     return moment(p.DATEPOST).format('DD MMM YYYY');
        //   }
        // },
      ],
      drawCallback: function (settings) {
        $("#table-ita thead").remove();
      },
    });

    dateYear();



  }, 700);

  setTimeout(() => {
    // let queryParams = new URLSearchParams(window.location.search);
    // queryParams.set("year", $("#itaYear").children("option:selected").val());
    // history.replaceState(null, null, "?" + queryParams.toString());


    tableitaeb = $("#tableitaeb").DataTable({
      order: [[0, "desc"]],
      lengthChange: false,
      searching: false,
      info: false,
      pageLength: 100,
      language: {
        info: "หน้า _PAGE_ ทั้งหมด _PAGES_ หน้า",
        search: "ค้นหาข่าว",
        zeroRecords: "ไม่พบข่าวที่ค้นหา",
        infoEmpty: "0 ข่าว",
        paginate: {
          first: "First",
          last: "Last",
          next: "ถัดไป",
          previous: "ย้อนกลับ",
        },
        processing: "กำลังโหลด",
      },
      processing: true,
      serverSide: true,
      ajax: function (data, callback) {

        let dataBody = { "year":  $.urlParam("year")|| '2025'};
        fetch(endPoint + pathAPI + "/news/itaebtype.php", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(dataBody),
        })
          .then((response) => response.json())
          .then((json) => {
            let news = json;
            // console.log(news);
            callback({
              recordsTotal: news.recordsTotal[0].TOTAL,
              recordsFiltered: news.recordsTotal[0].TOTAL,
              data: news.data,
            });
          });
      },
      columns: [
        {
          data: function (p, type, row) {
            return `${p.eb_no}`;
          },
        },
        {
          data: function (p, type, row) {
            return `<a href="?page=eb&eb=${p.id}&year=${p.year}">${p.eb_name}</a>`;
          },
        }
      ],
      drawCallback: function (settings) {
        $("#table-ita thead").remove();
      },
    });
    $("#itaYear").change(async function () {
      let queryParams = new URLSearchParams(window.location.search);
      queryParams.set("year", $("#itaYear").children("option:selected").val());
      history.replaceState(null, null, "?" + queryParams.toString());
      tableitaeb.ajax.reload();
    });
  }, 1000);
}

function dateYear() {
  $("#dateita").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years",
    languag: "en-th",
  });
}
