let current_page = 1;
let records_per_page = 3;

let total = 0;
async function setTotal(newsType) {
  total = await getTotalNew(newsType) 
}
// let objJson = [
//   { adName: "AdName 1" },
//   { adName: "AdName 2" },
//   { adName: "AdName 3" },
//   { adName: "AdName 4" },
//   { adName: "AdName 5" },
//   { adName: "AdName 6" },
//   { adName: "AdName 7" },
//   { adName: "AdName 8" },
//   { adName: "AdName 9" },
//   { adName: "AdName 10" }
// ]; 
// Can be obtained from another source, such as your objJson letiable



function prevPage() {
  if (current_page > 1) {
    current_page--;
    let valueSelected = $("#newsType").children("option:selected").val() || 1;
    changePage(current_page,valueSelected)
  }
}

function nextPage() {
  if (current_page < numPages()) {
    current_page++;
    let valueSelected = $("#newsType").children("option:selected").val() || 1;
    changePage(current_page,valueSelected)
  }
}

async function changePage(page, newsType) {
 
  let Type = newsType || 1;
  current_page = page

  // let btn_next = document.getElementById("btn_next");
  // let btn_prev = document.getElementById("btn_prev");
  // let listing_table = document.getElementById("listingTable");
  // let page_span = document.getElementById("page");

  // Validate page
  if (page < 1) page = 1;
  if (page > numPages()) page = numPages();
  console.log('numPages',numPages());
  // listing_table.innerHTML = "";

  // for (let i = (page - 1) * records_per_page; i < (page * records_per_page); i++) {
  //   listing_table.innerHTML += objJson[i].adName + "<br>";
  // }
  getAllNews(page.toString(), Type)
  // page_span.innerHTML = page;
  let listPage = $('.page');
  if (numPages() == 1) {
    $(listPage[1]).hide();
    $(listPage[2]).hide();
  } else
    if (page == numPages() || numPages() < 3) {
      $(listPage[2]).hide();
    } else {
      $(listPage[1]).show();
      $(listPage[2]).show();
    }

  if (page == 1) {
    for (let index = 0; index < listPage.length; index++) {
      const element = listPage[index];
      let el = $(element).children()
      if (index == 0) {
        $(element).addClass('active')
        $(el).text(page)
      } else {
        $(element).removeClass('active')
      }
      if (index == 1) {

        $(el).text(page + 1)
      }
      if (index == 2) {
        $(el).text(page + 2)
      }
    }
  } else
    if (page > 1) {
      for (let index = 0; index < listPage.length; index++) {
        const element = listPage[index];
        let el = $(element).children()
        if (index == 0) {
          $(el).text(page - 1)
        }
        if (index == 1) {
          $(element).addClass('active')
          $(el).text(page)
        } else {
          $(element).removeClass('active')
        }
        if (index == 2) {
          $(el).text(page + 1)
        }
      }
    }


  if (page == 1) {
    // btn_prev.style.visibility = "hidden";
    $('#btn_prev').addClass('disabled');
  } else {
    // btn_prev.style.visibility = "visible";
    $('#btn_prev').removeClass('disabled')
  }

  if (page == numPages()) {
    // btn_next.style.visibility = "hidden";
    $('#btn_next').addClass('disabled');
  } else {
    // btn_next.style.visibility = "visible";
    $('#btn_next').removeClass('disabled')
  }
}
 function numPages() {
 
  return Math.ceil(total / records_per_page);
}

